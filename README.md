# API Pick to light

Ce projet appartient au projet [pick to light](https://gitlab.com/ecam6/ptl_apps) et se base sur [Django Rest Framework](https://www.django-rest-framework.org/).

API qui gère la configuration du logiciel ainsi que l'accès à la base de donnée.

# Documentation

L'api se documente d'elle même une fois lancée depuis l'url : api/schema/swagger-ui/

# Variable d'environement

- DB_NAME (default: ptl)
- DB_USER (default: ptl)
- DB_PASSWORD (default: ecam6ptl2021)
- DB_HOST (default: postgres)
- DB_PORT (default: 5432

- AQUIWEB_DB_HOST (default: divalto)
- AQUIWEB_DB_NAME (default: aquiweb)
- AQUIWEB_DB_USER (default: user)
- AQUIWEB_DB_PWD (default: $)


# Déploiement, usage, configuration...

Ne peut pas être utilisé seul, se référé au [projet principal](https://gitlab.com/ecam6/ptl_apps)
