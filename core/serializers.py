from rest_framework import serializers
from core.models import Workstation, Line, Box, Article, Iolink, Event, Setting


class BoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Box
        fields = [
            "id",
            "line",
            "article",
            "article_name",
            "pos",
            "size",
            "col_size",
            "is_on",
        ]


class ArticleSerializer(serializers.ModelSerializer):
    boxes = BoxSerializer(many=True, required=False)

    class Meta:
        model = Article
        fields = ["id", "id_ext", "name", "boxes"]


class LineSerializer(serializers.ModelSerializer):
    boxes = BoxSerializer(many=True, required=False)

    class Meta:
        model = Line
        fields = ["id", "workstation", "pos", "is_full", "boxes"]


class IoLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Iolink
        fields = ["id", "ip"]


class WorkstationSerializer(serializers.ModelSerializer):
    lines = LineSerializer(many=True, required=False)

    class Meta:
        model = Workstation
        fields = ["id", "id_ext", "pos", "lines", "iolink", "iolink_port"]


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ["id", "id_ext", "id_workstation_ext", "id_article_ext"]


class SettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Setting
        fields = ["key", "value"]
