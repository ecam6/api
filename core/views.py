from rest_framework import status, viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
import psycopg2
import os
from core.models import Workstation, Line, Box, Article, Iolink, Event, Setting

from core.serializers import (
    WorkstationSerializer,
    LineSerializer,
    BoxSerializer,
    ArticleSerializer,
    IoLinkSerializer,
    EventSerializer,
    SettingSerializer,
)
from drf_spectacular.utils import extend_schema

# Create your views here.
class WorkstationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wrokstations to be viewed or edited.
    """

    queryset = Workstation.objects.all().order_by("pos")
    serializer_class = WorkstationSerializer
    # permission_classes = [permissions.IsAuthenticated] # TODO auth
    @extend_schema(responses={(200, "text/plain"): str, (500, "text/plain"): str})
    @action(detail=False, methods=["GET"], name="Sync workstation from the MES")
    def sync(self, request, pk=None):
        conn = None
        try:
            conn = psycopg2.connect(
                host=os.getenv("AQUIWEB_DB_HOST", "divalto"),
                database=os.getenv("AQUIWEB_DB_NAME", "aquiweb"),
                user=os.getenv("AQUIWEB_DB_USER", "user"),
                password=os.getenv("AQUIWEB_DB_PWD", "$"),
            )
            cur = conn.cursor()
            cur.execute(
                "SELECT id_equipement, id_equipement_erp FROM public.equipement WHERE id_localisationequipement = '3' AND is_active='true'"
            )
            rows = cur.fetchall()
            workstations = Workstation.objects.all()
            for row in rows:
                id_equipement = row[0]
                id_equipement_erp = row[1]
                workstation = [
                    w for w in workstations if str(id_equipement) == w.id_ext
                ]
                pos = str(id_equipement_erp)[1:]
                if workstation:
                    workstation[0].pos = pos
                    workstation[0].save()
                else:
                    Workstation.objects.create(pos=pos, id_ext=str(id_equipement))
            cur.close()
            conn.close()
        except Exception as err:
            if conn is not None:
                conn.close()
            return Response("Exception: {0}".format(err), status=500)

        return Response("OK", status=200)


class LineViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wrokstations to be viewed or edited.
    """

    queryset = Line.objects.all().order_by("id")
    serializer_class = LineSerializer
    # permission_classes = [permissions.IsAuthenticated] # TODO auth


class BoxViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wrokstations to be viewed or edited.
    """

    queryset = Box.objects.all().order_by("pos")
    serializer_class = BoxSerializer
    # permission_classes = [permissions.IsAuthenticated] # TODO auth


class ArticleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wrokstations to be viewed or edited.
    """

    queryset = Article.objects.all().order_by("name")
    serializer_class = ArticleSerializer
    pagination_class = None
    # permission_classes = [permissions.IsAuthenticated] # TODO auth
    @extend_schema(responses={(200, "text/plain"): str, (500, "text/plain"): str})
    @action(detail=False, methods=["GET"], name="Sync article from the MES")
    def sync(self, request, pk=None):
        conn = None
        try:
            conn = psycopg2.connect(
                host=os.getenv("AQUIWEB_DB_HOST", "vedivalto.ecam-eu.local"),
                database=os.getenv("AQUIWEB_DB_NAME", "aquiweb"),
                user=os.getenv("AQUIWEB_DB_USER", "ecam6sqlread"),
                password=os.getenv("AQUIWEB_DB_PWD", "7DD6REf$"),
            )
            cur = conn.cursor()
            cur.execute("SELECT id, libelle FROM public.element")
            rows = cur.fetchall()
            articles = Article.objects.all()
            for row in rows:
                id_ext = row[0]
                libelle = row[1]
                article = [art for art in articles if str(id_ext) == art.id_ext]
                if article:
                    article[0].name = libelle
                    article[0].save()
                else:
                    Article.objects.create(name=libelle, id_ext=id_ext)
            cur.close()
            conn.close()
        except Exception as err:
            if conn is not None:
                conn.close()
            return Response("Exception: {0}".format(err), status=500)

        return Response("OK", status=200)


class IolinkViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wrokstations to be viewed or edited.
    """

    queryset = Iolink.objects.all().order_by("ip")
    serializer_class = IoLinkSerializer
    # permission_classes = [permissions.IsAuthenticated] # TODO auth


class EventViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wrokstations to be viewed or edited.
    """

    queryset = Event.objects.all().order_by("id")
    serializer_class = EventSerializer
    # permission_classes = [permissions.IsAuthenticated] # TODO auth


class SettingViewSet(viewsets.ModelViewSet):
    queryset = Setting.objects.all().order_by("key")
    serializer_class = SettingSerializer
    # permission_classes = [permissions.IsAuthenticated] # TODO auth
