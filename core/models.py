from django.db import models
import uuid
from drf_spectacular.utils import extend_schema_field
from drf_spectacular.openapi import OpenApiTypes

# Create your models here.


class Article(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id_ext = models.TextField(unique=True)
    name = models.TextField()

    def __str__(self):
        return self.name


class Iolink(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    ip = models.GenericIPAddressField(unique=True)

    def __str__(self):
        return self.ip


class Workstation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id_ext = models.TextField(unique=True)
    pos = models.IntegerField(unique=True)
    iolink = models.OneToOneField(
        Iolink, on_delete=models.CASCADE, null=True, blank=True
    )
    iolink_port = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "Workstation " + str(self.pos)


class Line(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pos = models.IntegerField()
    workstation = models.ForeignKey(
        Workstation, related_name="lines", on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ("pos", "workstation")

    def __str__(self):
        return "Workstation " + str(self.workstation.pos) + " - Line " + str(self.pos)

    @property
    @extend_schema_field(OpenApiTypes.BOOL)
    def is_full(self):
        return sum(b.col_size for b in self.boxes.all()) >= 11


class Box(models.Model):
    SIZE_VIEW = {"B": "Big", "S": "Small"}
    SIZE = (
        ("B", "Big"),
        ("S", "Small"),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pos = models.IntegerField()
    size = models.CharField(choices=SIZE, max_length=1)
    line = models.ForeignKey(Line, related_name="boxes", on_delete=models.CASCADE)
    article = models.ForeignKey(
        Article, related_name="boxes", on_delete=models.CASCADE, null=True, blank=True
    )
    is_on = models.BooleanField(default=False)

    def __str__(self):
        return (
            "Workstation "
            + str(self.line.workstation.pos)
            + " - Line "
            + str(self.line.pos)
            + " - Box "
            + str(self.pos)
        )

    @property
    @extend_schema_field(OpenApiTypes.INT)
    def col_size(self):
        if self.size == "S":
            return 2
        else:
            return 3

    @property
    @extend_schema_field(OpenApiTypes.STR)
    def article_name(self):
        if self.article:
            return self.article.name
        return ""

    class Meta:
        unique_together = ("pos", "line")
        ordering = ["pos"]


class Event(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id_ext = models.TextField()
    id_workstation_ext = models.TextField()
    id_article_ext = models.TextField()


class Setting(models.Model):
    key = models.TextField(primary_key=True)
    value = models.TextField()
